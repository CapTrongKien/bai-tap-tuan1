package com.captrongkien.baitaptuan1.service;

import com.captrongkien.baitaptuan1.entity.MonHoc;

import java.util.List;

public interface MonHocService {
    MonHoc getMonHoc(int maMH);
    List<MonHoc> getAllMonHoc();
    void createMonHoc(MonHoc mh);
    void updateMonHoc(int maMH, MonHoc mh);
}
