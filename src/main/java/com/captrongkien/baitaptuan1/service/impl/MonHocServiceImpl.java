package com.captrongkien.baitaptuan1.service.impl;

import java.util.List;

import com.captrongkien.baitaptuan1.entity.MonHoc;
import com.captrongkien.baitaptuan1.repository.MonHocRepository;
import com.captrongkien.baitaptuan1.service.MonHocService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class MonHocServiceImpl implements MonHocService {

	@Autowired
	MonHocRepository mhRepository;

	@Override
	public MonHoc getMonHoc(int maMH) {
		return mhRepository.findByMaMH(maMH);
	}

	@Override
	public List<MonHoc> getAllMonHoc() {
		return mhRepository.findAll();
	}

	@Override
	public void createMonHoc(MonHoc mh) {
		mhRepository.save(mh);
	}

	@Override
	public void updateMonHoc(int maMH, MonHoc mh) {
		mh.setMaMH(maMH);
		mhRepository.save(mh);
	}
}
