package com.captrongkien.baitaptuan1.repository;

import java.util.List;

import com.captrongkien.baitaptuan1.entity.MonHoc;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MonHocRepository extends JpaRepository<MonHoc, Integer> {
	MonHoc findByMaMH(int maMH);

	List<MonHoc> findByTenMH(String tenMH);

}
