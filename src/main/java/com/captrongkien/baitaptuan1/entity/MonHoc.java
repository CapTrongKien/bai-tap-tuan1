package com.captrongkien.baitaptuan1.entity;

import javax.persistence.*;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;

@Entity
@Table(name = "mon_hoc")
public class MonHoc {
	@Id
	@GeneratedValue(strategy= GenerationType.IDENTITY)
	@Column(columnDefinition = "int")
	private int maMH;

	private String tenMH;

	private String gtMH;

	public int getMaMH() {
		return maMH;
	}

	public void setMaMH(int maMH) {
		this.maMH = maMH;
	}

	public String getTenMH() {
		return tenMH;
	}

	public void setTenMH(String tenMH) {
		this.tenMH = tenMH;
	}

	public String getGtMH() {
		return gtMH;
	}

	public void setGtMH(String gtMH) {
		this.gtMH = gtMH;
	}
}
