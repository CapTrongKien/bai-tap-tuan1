package com.captrongkien.baitaptuan1.controller;

import com.captrongkien.baitaptuan1.entity.MonHoc;
import com.captrongkien.baitaptuan1.service.MonHocService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/monhoc")
public class MonhocController {
	@Autowired
	MonHocService mhService;

	@GetMapping("/{maMH}")
	public MonHoc getOne(@PathVariable int maMH) {
		return mhService.getMonHoc(maMH);
	}

	@GetMapping
	public List<MonHoc> getAll() {
		return mhService.getAllMonHoc();
	}

	@PostMapping
	public void create(@RequestBody MonHoc mh) {
		mhService.createMonHoc(mh);
	}

	@PutMapping("/{maMH}")
	public void update(@PathVariable int maMH, @RequestBody MonHoc mh) {
		mhService.updateMonHoc(maMH, mh);
	}
}
